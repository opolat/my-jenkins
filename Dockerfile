FROM jenkins:alpine

USER root

ENV IVY_HOME=/cache \
    GRADLE_VERSION=4.2.1 \
    GRADLE_HOME=/usr/local/gradle \
    PATH=${PATH}:${GRADLE_HOME}/bin \
    GRADLE_USER_HOME=/gradle

RUN apk update && \
      apk upgrade && apk add openssl 

# Install gradle
WORKDIR /usr/local
RUN wget  https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip && \
    unzip gradle-$GRADLE_VERSION-bin.zip && \
    rm -f gradle-$GRADLE_VERSION-bin.zip && \
    ln -s $(pwd)/gradle-$GRADLE_VERSION/bin/gradle /usr/bin/gradle && \
    echo -ne "- with Gradle $GRADLE_VERSION\n" >> /root/.built && \
    apk add libstdc++ docker && rm -rf /var/cache/apk/*

